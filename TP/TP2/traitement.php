<?php

  define('TARGET_DIRECTORY', './photos/');
  if (!empty($_FILES['photo']))
  {
    move_uploaded_file($_FILES['photo']['tmp_name'], TARGET_DIRECTORY . $_FILES['photo']['name']);
    $photo = TARGET_DIRECTORY . $_FILES['photo']['name'];
  }

  if (isset($_POST['nom']) && isset($_POST['prix']) && isset($_POST['quantite']))
  {
    $nomProduit = htmlspecialchars($_POST['nom'], ENT_QUOTES);
    $prix = htmlspecialchars(round($_POST['prix'],2), ENT_QUOTES);
    $quantite = htmlspecialchars($_POST['quantite'], ENT_QUOTES);

    if (!preg_match('/^[a-zA-Z0-9 ]+$/', $nomProduit))
    {
      die('Le nom du produit n\'est pas valide, il ne peut contenir que des lettres et des chiffres.');
    }
    if ($prix < 0)
    {
      die('Ce prix n\'est pas valide !');
    }

    $file = fopen('mes_produits.csv', 'a');

    $product = array($nomProduit, $prix, $photo, $quantite);

    if(!fputcsv($file, $product, ';')) {fclose($file); die('Erreur lors de l\'enregistrement du produit'); }

    fclose($file);

    header('Location: index.php');
    exit();
  }
?>
