<!DOCTYPE html>

<html>
  <head>
    <title>Bon coin</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>

  <body>
    <header>
      <h1>Bon coin</h1>
    </header>

    <section>
      <?php
        include('ajout_produits.php');
        include('liste_produits.php');
      ?>
    </section>
  </body>
</html>
