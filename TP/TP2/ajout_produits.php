<form action="traitement.php" method="POST" enctype="multipart/form-data">
  <fieldset>
    <legend>Ajouter un produit</legend>
    <p>
      <label for="nom">Nom du produit</label><input type="text" name="nom" id="nom"/>
    </p>
    <p>
      <label for="photo">Photo</label><input type="file" name="photo" id="photo"/>
    </p>
    <p>
      <label for="prix">Prix</label><input type="text" name="prix" id="prix"/>
    </p>
    <p>
      <label for="quantite">Quantité</label><input type="number" name="quantite" id="quantite" min="0"/>
    </p>
    <p>
      <input type="submit" value="Valider" />
    </p>
  </fieldset>
</form>

