<!DOCTYPE html>
<html>

	<head>
		<title>Messenger</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>

	<body>
		
		<div id="page">
			<h1>Messenger</h1>

			<section id="send">
				<form action="traitement.php" method="POST" enctype="multipart/form-data">
					<fieldset>
						<p>
					      <label for="pseudo">Pseudo</label><input type="text" name="pseudo" id="pseudo"/>
					    </p>
					    <p>
					      <label for="message">Message</label><input type="text" name="message" id="message"/>
					    </p>
					    <p id="valider">
					      <input type="submit" value="Send" id="submit"/>
					    </p>
				  	</fieldset>
				</form>	
			</section>

			<section id="chat">
				<?php
					try
					{
						$bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'root', 'root');
					}
					catch (Exception $e)
					{
					    die('Erreur : ' . $e->getMessage());
					}

					$pseudo = htmlspecialchars($_POST['pseudo']);
					$message = htmlspecialchars($_POST['message']);

					$reponse = $bdd->query('SELECT pseudo, contenu FROM chat ORDER BY id');

					while ($donnees = $reponse->fetch())
					{
						echo '<p><strong>' . $pseudo . '</strong> : ' . $message . '</p>';
					}

					$reponse->closeCursor();	
				?>
			</section>
		</div>

	</body>

</html>