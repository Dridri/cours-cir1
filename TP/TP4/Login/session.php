<?php
session_start();

if(empty($_POST['username'])) 
{
    echo "Username field is required.";
} 
else 
{    
    if(empty($_POST['password'])) 
    {
        echo "Password field is required.";
    } 
    else 
    {    
        $username = htmlentities($_POST['username'], ENT_QUOTES, "ISO-8859-1");  
        $password = htmlentities($_POST['password'], ENT_QUOTES, "ISO-8859-1");

        try{
            $bdd = new PDO('mysql:host=localhost;dbname=events;charset=utf8','root', 'root');
        }catch(Exception $e){
            exit("Erreur" .$e -> getMessage());
        }

        $result = $bdd->prepare("SELECT Users.login, Users.password FROM events.Users WHERE login = :username");
        $result->execute(array(':username'=>$username));
        $data = $result->fetch();
        if(password_verify($password, $data['password']))
        {
            $_SESSION['username'] = $username;
            header("Location: ../Calendar/calendar.php");
        }
        else
        {
            $_SESSION['error'] = "Username or password is invalid.";
            header("Location: ../index.php");
        }
    }
}
?>