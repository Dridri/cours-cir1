<?php
	
	session_start();

	try{
        $bdd = new PDO('mysql:host=localhost;dbname=events;charset=utf8','root', 'root');
    }catch(Exception $e){
        exit("Erreur" .$e -> getMessage());
    }

    $username = htmlentities($_POST['username'], ENT_QUOTES, "ISO-8859-1");  
    $password = htmlentities($_POST['password'], ENT_QUOTES, "ISO-8859-1");
    $passwordHash = password_hash($password, PASSWORD_DEFAULT);
    $rank = htmlentities($_POST['rank'], ENT_QUOTES, "ISO-8859-1");

	$result = $bdd->prepare('INSERT INTO Users (login, password, rank) VALUES (?, ?, ?)');
	$result->execute(array($username, $passwordHash, $rank));	

	header("Location: ../index.php");
?>