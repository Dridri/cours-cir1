<form  class="cf" method="post" action="Login/session.php" enctype="multipart/form-data">
 
  <div class="login-page">
    
    <div class="form">
      
      <h1> LOGIN </h1>
      <form class="login-form">
        <input type="text" name="username" placeholder="Type your username"/>
        <input type="password" name="password" placeholder="Type your password"/>
        <?php 
            session_start();
            if(!empty($_SESSION['error']))
            {
                echo "<div class=\"error\">";
                echo htmlspecialchars($_SESSION['error']);
                echo "</div>"; 
                $_SESSION['error'] = "";
            }
        ?>
        <input type="submit" name="submit" value=" Login " id="submit">
        <p class="message">Not registered? <a href="Login/register.php">Create an account</a></p>
      </form>

    </div>

  </div>

</form>
